export class Helpers {

  constructor() { }

  static onlyNumber(value) {
    return value.replace(/[^\d]+/g, '');
  }

  static removeEmptyValues(obj) {
    for (var propName in obj) {
      if (!obj[propName] || obj[propName].length === 0) {
        delete obj[propName];
      } else if (typeof obj === 'object') {
        this.removeEmptyValues(obj[propName]);
      }
    }
    return obj;
  }

  static isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }
}
