import { AbstractControl, FormControl, Validators, FormGroup } from '@angular/forms';
import { Helpers } from './helpers';

export const validatePhone = (control) => {
  const regex = /^\(?\d{2}\)?[\s-]?[\s9]?\d{4}-?\d{4}$/;
  const regexPhone = /^\(?\d{2}\)?[\s-]\d{4}-?\d{4}$/;
  control.value = control.value.replace(/_/g,'');
  if(regex.test(control.value) || regexPhone.test(control.value) || control.value === null || control.value === undefined || control.value === ""){
    return null;
  }else{
    return { error: true };
  }
};

export const validateEmail = (control: AbstractControl) => {
  // tslint:disable-next-line:max-line-length
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (regex.test(control.value) || control.value === null || control.value === undefined || control.value === "") {
    return null;
  } else {
    return {
      error: true
    };
  }
};

export const validatorsCep = (control: AbstractControl) => {
  const value = Helpers.onlyNumber(control.value);
  const regex = /^[0-9]{8}$/;
  if (regex.test(value) || value === null || value === undefined || value === "") {
    return null;
  } else {
    return {
      error: true
    };
  }
};
