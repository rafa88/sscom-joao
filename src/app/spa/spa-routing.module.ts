import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SpaComponent } from './spa.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: SpaComponent,
        children: [
          // View Modules
          { path: '', loadChildren: 'app/spa/home/home.module#HomeModule'},
          { path: 'clientes', loadChildren: 'app/spa/clients/clients.module#ClientsModule' },
        ]
      }
    ])
  ],
  exports: [RouterModule]
})

export class SpaRoutingModule { }
