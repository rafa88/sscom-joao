import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClientsComponent } from './clients.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsFormComponent } from './clients-form/clients-form.component';
import { ClientsShowComponent } from './clients-show/clients-show.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ClientsComponent, data: { title: 'Mobile' },
        children: [
          { path: '', component: ClientsListComponent },
          { path: 'cadastrar', component: ClientsFormComponent },
          { path: ':id', component: ClientsShowComponent },
          { path: ':id/editar', component: ClientsFormComponent },
        ]
      },
    ]),
  ],
  exports: [RouterModule]
})
export class ClientsRoutingModule {}
