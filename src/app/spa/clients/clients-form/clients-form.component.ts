import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { validatePhone, validatorsCep } from '../../../shared/class/validator.method';
import { ValidateForm } from '../../../shared/class/validate-form';
import { ClientsService } from '../../shared/services/clients/clients.service';
import { Helpers } from '../../../shared/class/helpers';
import { routerTransition } from '../../shared/animations/router-transition';

@Component({
  selector: 'sscom-clients-form',
  templateUrl: './clients-form.component.html',
  styleUrls: ['./clients-form.component.scss'],
  animations: [routerTransition],
  host: { '[@routerTransition]': '' }
})
export class ClientsFormComponent implements OnInit {

  private subscriptions: Subscription[] = [];
  
  public validateForm = ValidateForm;

  public clientForm: FormGroup;
  public maskPhone;
  public loading: boolean = true;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private clientsService: ClientsService
  ) { }

  ngOnInit() {
    if (this.isUpdate()) {
      this.show();
    } else {
      this.createFormClient();
    }
  }

  show() {
    this.subscriptions.push(
      this.clientsService.show(this.route.snapshot.params.id).subscribe(res => {
        this.createFormClient(res);
        this.changeMask(res.phone);
      })
    );
  }

  createFormClient(params?: any) {
    this.loading = false;
    this.clientForm = this.formBuilder.group({
      name: [params ? params.name : '', [Validators.required]],
      email: [params ? params.email : '', [Validators.required, Validators.email]],
      phone: [params ? Helpers.onlyNumber(params.phone) : '', [Validators.required, validatePhone]],
      cep: [params ? Helpers.onlyNumber(params.cep) : '', [Validators.required, validatorsCep]],
      line1: [params ? params.line1 : ''],
      district: [params ? params.district : ''],
      number: [params ? params.number : ''],
      state: [params ? params.state : ''],
      city: [params ? params.city : ''],
      status: [params ? params.status : 'active', [Validators.required]],
    });
  }

  changeMask(value) {
    const regex = /^[1-9]{2}9/;
    if (regex.test(Helpers.onlyNumber(value))) {
      this.maskPhone = { mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/], guide: false };
    } else {
      this.maskPhone = { mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/], guide: false };
    }
  }

  receiveAddress(address) {
    this.clientForm.get('line1').setValue(address.logradouro);
    this.clientForm.get('city').setValue(address.localidade);
    this.clientForm.get('state').setValue(address.uf);
    this.clientForm.get('district').setValue(address.bairro);
  }

  sendForm() {
    if ( this.clientForm.valid && this.isUpdate() ) {
      this.update();
    } else if (this.clientForm.valid && !this.isUpdate()) {
      this.create();
    } else {
      this.validateForm.validateAllFormFields(this.clientForm);
    }
  }

  create() {
    this.loading = true;
    this.subscriptions.push(
      this.clientsService.post(this.valueSend()).subscribe(res => {
        this.router.navigate(['/clientes']);
      })
    );
  }

  update() {
    this.loading = true;
    this.subscriptions.push(
      this.clientsService.put(this.valueSend(), this.route.snapshot.params.id).subscribe(res => {
        this.router.navigate(['/clientes']);
      })
    );
  }

  valueSend() {
    const object = JSON.parse(JSON.stringify(this.clientForm.value));
    object.phone = Helpers.onlyNumber(object.phone);
    object.cep = Helpers.onlyNumber(object.cep);
    return object;
  }

  isUpdate(): boolean {
    return this.route.snapshot.params.id ? true : false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

}
