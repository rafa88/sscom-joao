import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { CepService } from '../../shared/services/cep/cep.service';
import { ValidateForm } from '../../../shared/class/validate-form';
import { Helpers } from '../../../shared/class/helpers';

@Component({
  selector: 'sscom-search-cep',
  templateUrl: './search-cep.component.html',
  styleUrls: ['./search-cep.component.scss']
})
export class SearchCepComponent implements OnInit {

  @Input() cep: FormGroup;
  @Output() address = new EventEmitter;

  private subscriptions: Subscription[] = [];

  public validateForm = ValidateForm;
  public cepMask = { mask: [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/], guide: false };

  constructor(
    private cepService: CepService
  ) { }

  ngOnInit() {
  }

  getZip(value) {
    const regex = /^[0-9]{8}$/;
    if ( regex.test( Helpers.onlyNumber(value) ) ) {
      this.subscriptions.push(
        this.cepService.getaddress(value).subscribe( res => {
          if ( !res.erro ) {
            this.address.emit(res);
          }
        })
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

}
