import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatRadioModule,
  MatSelectModule,
  MatOptionModule,
  MatSlideToggleModule,

  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
} from '@angular/material';

import { ClientsComponent } from './clients.component';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsShowComponent } from './clients-show/clients-show.component';
import { ClientsFormComponent } from './clients-form/clients-form.component';
import { ClientsRoutingModule } from './clients-routing.module';
import { SearchCepComponent } from './search-cep/search-cep.component';
import { SharedSpaModule } from '../shared/shared.spa.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule.forRoot(),

    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatRadioModule,
    MatSelectModule,
    MatOptionModule,
    MatSlideToggleModule,

    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,

    ReactiveFormsModule,
    FormsModule,
    TextMaskModule,
    SharedSpaModule,
    ClientsRoutingModule
  ],
  declarations: [ClientsComponent, ClientsListComponent, ClientsShowComponent, ClientsFormComponent, SearchCepComponent]
})
export class ClientsModule { }
