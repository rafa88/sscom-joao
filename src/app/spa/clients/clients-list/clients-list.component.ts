import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { ClientsService } from '../../shared/services/clients/clients.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Clients } from '../../shared/models/clients.model';
import { routerTransition } from '../../shared/animations/router-transition';
import { validateEmail } from '../../../shared/class/validator.method';
import { Helpers } from '../../../shared/class/helpers';

@Component({
  selector: 'sscom-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss'],
  animations: [routerTransition],
  host: { '[@routerTransition]': '' }
})
export class ClientsListComponent implements OnInit {

  private subscriptions: Subscription[] = [];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  public displayedColumns = ['name', 'email', 'cep', 'phone', 'status'];
  public clients: MatTableDataSource<Clients>;;
  public resultsLength = 0;
  public loading = true;

  public searchForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private clientsService: ClientsService
  ) { }

  ngOnInit() {
    this.listeningChangeRoutes();
    this.createFormSearch();
    this.paginator.pageSize = 8;
  }

  createFormSearch() {
    this.searchForm = this.formBuilder.group({
      search: [''],
      email: ['', [validateEmail]],
      cep: [''],
    });
  }

  listeningChangeRoutes(): void {
    this.list(this.route.snapshot.queryParams);
    this.subscriptions.push(
      this.router.events
        .filter(res => res instanceof NavigationEnd)
        .subscribe((res: NavigationEnd) => {
          this.list(this.route.snapshot.queryParams);
        })
    );
  }

  list(params?: any) {
    this.subscriptions.push(
      this.clientsService.index(params).subscribe(res => {
        this.loading = false;
        this.resultsLength = res.length;
        this.clients = new MatTableDataSource(this.filter(res));
        this.clients.paginator = this.paginator;
      })
    );
  }

  filter(res) {
    return res.map(element => {
      return new Clients(element);
    });
  }

  redirect(id) {
    this.router.navigate([`/clientes/${id}`])
  }

  sendForm() {
    const queryParams = Helpers.removeEmptyValues(this.searchForm.value);
    if (queryParams) {
      this.router.navigate(['/clientes'], { queryParams: queryParams });
    }
  }
  
  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

}
