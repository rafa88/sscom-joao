import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ClientsService } from '../../shared/services/clients/clients.service';
import { Clients } from '../../shared/models/clients.model';
import { routerTransition } from '../../shared/animations/router-transition';

@Component({
  selector: 'sscom-clients-show',
  templateUrl: './clients-show.component.html',
  styleUrls: ['./clients-show.component.scss'],
  animations: [routerTransition],
  host: { '[@routerTransition]': '' }
})
export class ClientsShowComponent implements OnInit {

  private subscriptions: Subscription[] = [];

  public client;
  public loading: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private clientsService: ClientsService
  ) { }

  ngOnInit() {
    this.show();
  }

  show() {
    this.subscriptions.push(
      this.clientsService.show(this.route.snapshot.params.id).subscribe(res => {
        this.client = new Clients(res);
        this.loading = false;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
  }

}
