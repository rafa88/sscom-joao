import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SpaComponent } from './spa.component';
import { SpaRoutingModule } from './spa-routing.module';
import { SharedSpaModule } from './shared/shared.spa.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SpaRoutingModule,
    SharedSpaModule
  ],
  declarations: [
    SpaComponent
  ]
})
export class SpaModule { }
