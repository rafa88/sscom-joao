import { EnumFunction } from "../decorators";

export class Clients {

  public id: number;
  public name: string;
  public email: string;
  public cep: string;
  public line1: string;
  public district: string;
  public state: string;
  public city: string;
  public phone: string;
  public createdAt: Date;
  private _status: string;

  constructor(params?: any) {
    if (params) {  this.setParams(params); }
    this.createdAt = (this.createdAt !== undefined ? new Date(this.createdAt) : new Date());
  }

  setParams(params: any): void {
    for (const [key, value] of Object.entries(params)) {
      this[key] = value;
    }
  }

  @EnumFunction({
    'active': 'Ativo',
    'archive': 'Arquivado'
  })
  get status() { return this._status; }
  set status(value: any) { }

  fullAddress(): string {
    const line1 = this.line1 ? `${this.line1} - ` : '';
    const district = this.district ? `${this.district} - ` : '';
    const city = this.city || '';
    const state = this.state ? `/${this.state}` : '';
    return line1 + district + city + state;
  }

}
