import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ClientsService } from './services/clients/clients.service';
import { CepService } from './services/cep/cep.service';
import { PaginationComponent } from './pagination/pagination.component';
import { HeaderService } from './services/header/header.service';
import { PhonePipe } from './pipes/phone.pipe';
import { CepPipe } from './pipes/cep.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    PaginationComponent,
    PhonePipe,
    CepPipe
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    PaginationComponent,
    PhonePipe,
    CepPipe
  ],
  providers: [
    HeaderService,
    ClientsService,
    CepService
  ]
})
export class SharedSpaModule { }
