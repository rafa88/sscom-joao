import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DataHandler } from '../../../../shared/class/data-handler';
import { environment } from '../../../../../environments/environment';
import { HeaderService } from '../header/header.service';

@Injectable()
export class ClientsService {

  constructor(
    private http: Http,
    private headerService: HeaderService,
  ) { }

  index(params?: any): Observable<any> {
    return this.http.get(`${environment.endpoint}/clients?limit=${environment.perPage}&sortBy=createdAt&order=asc`, this.headerService.setHeader(params))
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

  show(id): Observable<any> {
    return this.http.get(`${environment.endpoint}/clients/${id}`)
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

  count(): Observable<any> {
    return this.http.get(`${environment.endpoint}/clients`)
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

  post(data): Observable<any> {
    return this.http.post(`${environment.endpoint}/clients`, data)
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

  put(data, id): Observable<any> {
    return this.http.put(`${environment.endpoint}/clients/${id}`, data)
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

}
