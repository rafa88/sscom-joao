import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DataHandler } from '../../../../shared/class/data-handler';

@Injectable()
export class CepService {

  constructor( private http: Http ) { }

  getaddress(cep): Observable<any> {
    return this.http.get(`https://viacep.com.br/ws/${cep}/json`)
      .map(DataHandler.handlerData)
      .catch(DataHandler.handlerError);
  }

}
