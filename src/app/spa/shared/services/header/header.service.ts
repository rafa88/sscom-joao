import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';

@Injectable()
export class HeaderService {
  session: boolean;
  constructor() { }

  setHeader(params?: any): RequestOptions {
    const headers: Headers = new Headers();
    const options = new RequestOptions({ headers: headers, params: params });
    return options;
  }

}
