import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sscom-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'sscom';

  constructor() {}
  
  ngOnInit() {

  }
}
