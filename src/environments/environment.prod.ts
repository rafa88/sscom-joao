export const environment = {
  production: true,
  endpoint: 'http://5b788383b85997001447859a.mockapi.io/api/v1',
  perPage: 8,
  pagesToShow: 10
};
